from django.contrib import admin


from .models import Ingredient, Product, Coupon, News, DiscountInfo, Subscribe, Contact

admin.site.register(Ingredient)
admin.site.register(Product)
admin.site.register(Coupon)
admin.site.register(News)
admin.site.register(DiscountInfo)
admin.site.register(Subscribe)
admin.site.register(Contact)
