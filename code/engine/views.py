from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from .models import Product, Cart, Profile, Address, Coupon, Order, News, DiscountInfo, Contact
from .form import NameForm, ConfirmForm, ProfileForm, AddressForm, CouponForm, SubscribeForm, FeedBackForm
from uuid import uuid4
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json
from random import randint


def index(request):
    ingredient_list = {}
    products = Product.objects.all()
    name_form = NameForm()
    #name_form = ConfirmForm()
    subscribe_form = SubscribeForm()

    for product in products:
        tmp = {}
        ingredients = product.composition.all()
        for ingredient in ingredients:
            tmp[ingredient.id] = ingredient.cnt
        ingredient_list[product.id] = json.dumps(tmp)

    return render(request, 'index.html', {
        'products': products,
        'name_form': name_form,
        'user': request.user,
        'ingredient_list': ingredient_list,
        'subscribe_form': subscribe_form,
    })


def detail(request, product_id):
    burger = get_object_or_404(Product, pk=product_id)
    name_form = NameForm()

    return render(request, 'detail.html', {
        'burger': burger,
        'name_form': name_form,
    })


def auth(request):
    if request.method == 'POST':
        name_form = NameForm(request.POST)
        try:
            if name_form.is_valid():
                user = User.objects.get(username=name_form.cleaned_data.get('phone_or_email'))
                form = ConfirmForm(initial={'username': user.username})
                return render(request, 'confirm.html', {
                    'form': form
                })
            else:
                return HttpResponse("{}".format(name_form.errors))
        except User.DoesNotExist:
            user = User.objects.create_user(username=form.cleaned_data.get('phone_or_email'), email='', password=123456)
            user.is_staff = False
            user.is_superuser = False
            user.save()

            user_profile = Profile()
            user_profile.user_id = user
            user_profile.save()

            return render(request, 'confirm.html', {
                'form': ConfirmForm(initial={'username': form.cleaned_data.get('phone_or_email')})
            })


def confirm(request):
    if request.method == 'POST':
        form = ConfirmForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=name, password=password)
            if user is not None:
                login(request, user)
                return JsonResponse({'user_name': user.username})
                #return HttpResponseRedirect('/')
            else:
                return HttpResponse('False')


def account_logout(request):
    logout(request)
    return HttpResponseRedirect('/')


def cart(request):
    user_id = __get_user_id_or_session_id(request)
    cart = Cart.objects.filter(user_id=user_id)

    try:
        user_address = Address.objects.filter(user_id=__get_user_id_or_session_id(request))
    except Http404:
        pass

    address_form = AddressForm()
    name_form = NameForm()
    coupon_form = CouponForm()

    return render(request, 'cart.html', {
        'products': cart,
        'name_form': name_form,
        'address': address_form,
        'user_address': user_address,
        'coupon_form': coupon_form,
    })


@csrf_exempt
def cart_add(request):
    if request.method == 'POST':
        user_id = __get_user_id_or_session_id(request)

        product = Cart()
        product.user_id = user_id
        product.product_id = get_object_or_404(Product, pk=request.POST.get('id'))
        product.product_options_list = request.POST.get('product_options_list')
        product.cnt = request.POST.get('cnt')
        product.save()

        return JsonResponse({'status': True})


@csrf_exempt
def cart_update(request):
    if request.method == 'POST':
        cart = get_object_or_404(Cart, pk=request.POST.get('id'))
        cart.cnt = request.POST['cnt']
        cart.product_options_list = request.POST.get('product_options_list')
        cart.save()

        return JsonResponse({'status': True})


@csrf_exempt
def cart_remove(request):
    if request.method == 'POST':
        user_id = __get_user_id_or_session_id(request)

        item = Cart.objects.filter(pk=request.POST.get('id'), user_id=user_id)
        item.delete()

        return JsonResponse({'status': True})


def menu(request):
    products = Product.objects.all()
    name_form = NameForm()

    return render(request, 'menu.html', {
        'products': products,
        'name_form': name_form,
        'user': request.user,
    })


def profile(request):
    user_address = ''
    order_list = ''
    user_profile = Profile.objects.get(user_id=__get_user_id_or_session_id(request))
    try:
        user_address = Address.objects.filter(user_id=__get_user_id_or_session_id(request))
        user_orders = Order.objects.filter(user_id=__get_user_id_or_session_id(request))

        order_list = _get_product_list_from_order(user_orders)
    except Http404:
        pass

    profile_form = ProfileForm(instance=user_profile)
    address_form = AddressForm()
    feed_back_form = FeedBackForm()

    name_form = NameForm()

    return render(request, 'profile.html', {
        'profile': profile_form,
        'name_form': name_form,
        'address': address_form,
        'user_address': user_address,
        'user_orders': order_list,
        'feed_back_form': feed_back_form
    })


def feedback(request):
    if request.method == 'POST':
        feed_back_form = FeedBackForm(request.POST)

        if feed_back_form.is_valid():
            user_id = __get_user_id_or_session_id(request)
            feed_back_form.save(False)
            feed_back_form.order_id = get_object_or_404(Order, pk=request.POST, user_id=user_id)
            return JsonResponse({'status': True})

    return JsonResponse({'status': False})


def _get_product_list_from_order(user_orders):
    order_list = list()
    for user_order in user_orders:
        order = dict()
        order['id'] = user_order.id
        order['address'] = user_order.address_id.address + \
                                str(user_order.address_id.apartments) + \
                                str(user_order.address_id.entrance) + \
                                str(user_order.address_id.floor)
        order['cash_back'] = user_order.cash_back
        order['date_order_created'] = user_order.date_order_created
        order['payment_type'] = user_order.payment_type
        order['percentage'] = user_order.discount_id.percentage if user_order.discount_id else ''
        order['status'] = user_order.status
        order['product_list'] = list()

        product_ids = list()
        cart_items = json.loads(user_order.cart)

        for item in cart_items:
            fields = item.get('fields')
            product_ids.append(fields['product_id'])

        products = Product.objects.filter(pk__in=product_ids)

        for product in products:
            for item in cart_items:
                fields = item.get('fields')
                if fields['product_id'] == product.id:
                    total = 0
                    options_list = json.loads(fields['product_options_list'])
                    for ingredient in product.composition.all():
                        for id, cnt in options_list.items():
                            if id == ingredient.id and cnt > ingredient.cnt:
                                total += (cnt - ingredient.cnt) * ingredient.price

                    order['product_list'].append({
                        'name': product.name,
                        'cnt': fields['cnt'],
                        'price': (product.price + total) * fields['cnt']
                    })

        order_list.append(order)

    return order_list


def profile_save(request):
    if request.method == 'POST':
        form = None
        form_type = request.POST.get('profile_type')
        user_id = __get_user_id_or_session_id(request)

        if form_type == 'profile':
            profile = Profile.objects.get(user_id=user_id)
            form = ProfileForm(request.POST, instance=profile)
        elif form_type == 'address':
            try:
                address = request.POST.get('address')
                apartments = request.POST.get('apartments')
                entrance = request.POST.get('entrance')
                floor = request.POST.get('floor')

                get_object_or_404(Address, user_id=user_id, address=address, apartments=apartments, entrance=entrance,
                                  floor=floor)
                return JsonResponse({'status': False, 'message': 'Already exist'})
            except Http404:
                addr = Address(user_id=user_id)
                form = AddressForm(request.POST, instance=addr)

        if form is not None and form.is_valid():
            form.user_id = __get_user_id_or_session_id(request)
            form.save()
            return JsonResponse({'status': True})

        return JsonResponse({'status': False})


@csrf_exempt
def address_remove(request):
    if request.method == 'POST':
        user_id = __get_user_id_or_session_id(request)
        address_id = request.POST.get('address_id')
        address = Address.objects.get(pk=address_id, user_id=user_id)
        address.delete()
        return JsonResponse({'status': True})

    return JsonResponse({'status': False})


def coupon_set(request):
    try:
        coupon_name = request.POST.get('coupon_name')
        coupon = get_object_or_404(Coupon, coupon_name=coupon_name)
        return JsonResponse({'status': True, 'payload': coupon.percentage, 'id': coupon.id})
    except Http404:
        return JsonResponse({'status': False})


def order(request):
    if request.method == 'POST':
        user_id = __get_user_id_or_session_id(request)
        address_form = AddressForm(request.POST)
        payment_type = request.POST.get('payment_type')
        cart_data = Cart.objects.filter(user_id=user_id)
        discount = None

        try:
            discount = Coupon.objects.get(pk=request.POST.get('discount_id'))
        except Coupon.DoesNotExist:
            pass

        if address_form.is_valid():
            address, created = Address.objects.get_or_create(
                user_id=user_id,
                address=address_form.cleaned_data.get('address'),
                apartments=address_form.cleaned_data.get('apartments'),
                entrance=address_form.cleaned_data.get('entrance'),
                floor=address_form.cleaned_data.get('floor')
            )

        order = Order(
            discount_id=discount,
            address_id=address,
            user_id=user_id,
            cart=serializers.serialize('json', cart_data, fields=('product_id',
                                                                  'product_options_list',
                                                                  'cnt',
                                                                  'total_price')),
            payment_type=payment_type
        )

        order.clean()
        order.save()
        cart_data.delete()

        return JsonResponse({'status': True})


def is_member(user):
    return user.groups.filter(name='StaffAdmin').exists()


@user_passes_test(is_member)
@login_required(login_url='/auth')
def orders_control(request):
    user_orders = Order.objects.all()
    product_list = _get_product_list_from_order(user_orders)
    return render(request, 'orders_control.html', {
        'user_orders': product_list,
    })


@csrf_exempt
def order_repeat(request):
    user_id = __get_user_id_or_session_id(request)
    user_order = Order.objects.get(user_id=user_id, pk=request.POST.get('order_id'))
    cart_items = json.loads(user_order.cart)

    for item in cart_items:
        fields = item.get('fields')
        cart = Cart(
            user_id=user_id,
            product_id=Product.objects.get(pk=fields['product_id']),
            product_options_list=fields['product_options_list'],
            cnt=fields['cnt'],
            total_price=fields['total_price']
        )
        cart.save()
        return JsonResponse({'status': True})

    return JsonResponse({'status': False})


def news(request):
    news = News.objects.all()
    subscribe_form = SubscribeForm()
    return render(request, 'news.html', {
        'news': news,
        'subscribe_form': subscribe_form,
    })


def discount(request):
    discounts = DiscountInfo.objects.all()
    subscribe_form = SubscribeForm()
    return render(request, 'discount.html', {
        'discounts': discounts,
        'subscribe_form': subscribe_form
    })


def news_detail(request, news_id):
    news = News.objects.get(pk=news_id)
    return render(request, 'news_detail.html', {
        'news': news,
    })


def contacts(request):
    address = Contact.objects.get(pk=1)
    return render(request, 'contacts.html', {
        'address': address,
    })


def subscribe(request):
    if request.method == 'POST':
        subscribe_form = SubscribeForm(request.POST)

        if subscribe_form.is_valid():
            subscribe_form.save()
            return JsonResponse({'status': True})

    return JsonResponse({'status': False})


def __get_user_id_or_session_id(request):
    if request.user is not None and request.user.is_authenticated:
        user_id = request.user.id
    elif request.COOKIES.get('session_id'):
        user_id = request.COOKIES.get('session_id')
    else:
        user_id = uuid4()
    return user_id
