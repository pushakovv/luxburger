from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django import forms
from .models import Profile, Address, Coupon, Subscribe, FeedBack


class NameForm(forms.Form):
    phone_regexp = RegexValidator(
        regex='(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{5}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})',
        message='Не валидный логин , введите почту или телефон :('
    )
    phone_or_email = forms.CharField(
            max_length=30,
            required=True,
            label='Телефон или почта',
            widget=forms.TextInput(attrs={
                'class': 'js-name-or-email'
            })
    )

    def clean(self):
        login = self.cleaned_data.get('phone_or_email')
        if self.phone_regexp(login):
            return self.cleaned_data


class ConfirmForm(forms.Form):
    username = forms.CharField(widget=forms.HiddenInput)
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['name', 'email', 'phone', 'born']


class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = ['address', 'apartments', 'entrance', 'floor']


class CouponForm(forms.ModelForm):
    class Meta:
        model = Coupon
        fields = ['coupon_name']


class SubscribeForm(forms.ModelForm):
    class Meta:
        model = Subscribe
        fields = ['email']


class FeedBackForm(forms.ModelForm):
    rating_score = (
        (u'1', u'Bad'),
        (u'2', u'Good'),
        (u'3', u'Accelant'),
    )
    rating = forms.ChoiceField(choices=rating_score)

    class Meta:
        model = FeedBack
        fields = ['rating', 'comment']
