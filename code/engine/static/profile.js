(function () {
    window.Profile = new function () {
        // More detail u can find an link bellow
        // https://dadata.ru/api/suggest/address/
        var token = '2d083cb8e63b1d962b18c24448a47d360d2017ab';
        var jsLastOrder = '.js-last-order';
        var jsAddressBlock = '.js-address-block';
        var jsIdAddress = '#id_address';

        var repeatUrl = '/order/repeat/';
        var removeUrl = '/address/remove/';

        this.ReOrder = function (event) {
            var orderId = $(event.target).parents(jsLastOrder).data('order-id');

            $.post(repeatUrl, {
                'order_id': orderId
            }, function (res) {

            })
        };
        
        this.AddressRemove = function (removeBtn) {
            if (confirm('Are u shure?')) {
                var addressId = $(removeBtn).data('address-id');
                $.post(removeUrl, {
                    address_id: addressId,
                },function (res) {
                    if (res.status == true) {
                        $(removeBtn).parents(jsAddressBlock).remove();
                    }
                });
            }
        };

        function _initEvents() {
            $(jsIdAddress).suggestions({
                token: token,
                type: "ADDRESS",
                /* Вызывается, когда пользователь выбирает одну из подсказок */
                onSelect: function (suggestion) {
                    console.log(suggestion);
                }
            });
        }

        _initEvents();
    };
})();