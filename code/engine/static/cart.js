(function() {
    window.Cart = new function () {
        var jsProduct = '.js-product';
        var jsTotalPayment = '.js-total-payment';
        var jsDiscount = '.js-discount';
        var jsCashBack = '.js-cash-back';
        var jsProductCnt = '.js-product-cnt';
        var jsOptions = '.js-options';
        var jspPoductPrice = '.js-product-price';
        var jsTotalPaymentWithDiscount = '.js-total-payment-with-discount';
        var jsPaymentMethod = '.js-payment-method';
        var jsCartItems = '.js-cart-items';

        this.promoCode = 0;

        this.Add = function (product) {
            var $product = $(product).parents(jsProduct);
            var _productId = $product.data('product-id');
            var _productCnt = $product.find(jsProductCnt).val();
            var _productOptionList = $product.find('.js-product-options-list').val();

            $.post('/cart/add/', {
                'id': _productId,
                'cnt': _productCnt,
                'product_options_list': _productOptionList
            }, function (data) {
                if (data.status) {
                    alert('Burger add');
                }
            });
        };

        this.Remove = function (cartId) {
            var _self = this;
            var _productId = $(cartId).parents(jsProduct).data('product-id');
            var _product = cartId;

            $.post('/cart/remove/', {
                'id': _productId
            }, function (data) {
                if (data.status) {
                    $(_product).parents(jsProduct).remove();

                    if ($(_product).parents(jsProduct).length === 1) {
                        $(jsCartItems).html('<h1>Cart is empty</h1>')
                    }

                    _self.RecalcCart();
                }
            });
        };

        this.Update = function(product) {
            var _productOptionListJson = {};
            var $product = $(product).parents(jsProduct);
            var _productId = $product.data('product-id');
            var _productCnt = $(product).val();
            var _productOptionList = $product.find(jsOptions);

            _productOptionList.each(function (idx, data) {
                var optionsRow = $(data);
                _productOptionListJson[optionsRow.attr('name')] = optionsRow.val();
            });

            $.post('/cart/update/', {
                'id': _productId,
                'cnt': _productCnt,
                'product_options_list': JSON.stringify(_productOptionListJson)
            }, function (data) {

            });
        };

        this.RecalcCart = function () {
            var percentage = 4;
            var cartTotalPrice = 0;
            var discount;
            var finalProductPrice;

            $(jsProduct).each(function(idx, elem) {
                var productPrice = 0;
                var ingredientsPrice = 0;
                var $elem = $(elem);
                var productInitialPrice = $elem.data('initial-price');
                var productCnt = $elem.find(jsProductCnt).val();
                var options = JSON.parse($elem.find('.js-product-options-list').val());

                for(var key in options) {
                    var productOptionField = $elem.find('input[name=' + key + ']');
                    var initialPrice = productOptionField.data('initial-price');
                    var initialCnt = productOptionField.data('initial-cnt');

                    productOptionField.val(options[key]);

                    if (options[key] > initialCnt) {
                        ingredientsPrice += (options[key] - initialCnt) * initialPrice
                    }
                }

                cartTotalPrice += productPrice = (parseFloat(productInitialPrice) + ingredientsPrice) * productCnt;

                $(elem).find(jspPoductPrice).text(productPrice)
            });

            discount = this.promoCode ? (cartTotalPrice / 100) * this.promoCode.payload : this.promoCode;
            finalProductPrice = cartTotalPrice - discount;

            if (discount) {
                $(jsTotalPayment).addClass('product__discount_on');
                $(jsTotalPaymentWithDiscount).text(finalProductPrice);
            } else {
                $(jsTotalPayment).removeClass('product__discount_on');
                $(jsTotalPaymentWithDiscount).text('');
            }

            $(jsDiscount).text(discount);
            $(jsTotalPayment).text(cartTotalPrice);
            $(jsCashBack).text((finalProductPrice / 100) * percentage);
        };

        this.SetAddressFromList = function (elem) {
            var address = $(elem).prev().text().split('|');
            $('.js-address')
                    .find('input[name="address"], input[name="apartments"], input[name="entrance"], input[name="floor"]')
                    .each(function (idx, elem) {
                        $(elem).val(address[idx])
                    });
        };

        this.setCoupon = function (event, form) {
            event.preventDefault();
            var self = this;
            var couponName = $(form).find('input[name="coupon_name"]').val();
            var csrf = $(form).find('input[name="csrfmiddlewaretoken"]').val();

            $.post('/coupon/set/', {
                'coupon_name': couponName,
                'csrfmiddlewaretoken': csrf
            }, function (data) {
                self.promoCode = data;
                self.RecalcCart();
            });
        };

        this.UnsetCoupon = function (event) {
            event.preventDefault();
            this.promoCode = 0;
            this.RecalcCart();
        };

        this.Order = function (event, form) {
            event.preventDefault();
            var t = {};
            var form = $(form).serializeArray();
            for (var idx in form) {
                t[form[idx].name] = form[idx].value;
            }
            t.discount_id = this.promoCode ? this.promoCode.id : this.promoCode;
            t.payment_type = $(jsPaymentMethod + ":checked").val();

            $.post('/order/', t, function () {
                window.location.pathname = '/profile/';
            });
        };
    };
})();