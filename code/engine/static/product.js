(function () {
    window.Product = new function () {
        var product = '.js-product';
        var productCnt = '.js-product-cnt';
        var productPrice = '.js-product-price';
        var productOptionsList = '.js-options';
        var productOptionsHideField = '.js-product-options-list';

        function _initEvents() {
            $(productCnt).change(function(event) {
                _recalcProductPrice(event);
            });
        }

        function _recalcProductPrice(event) {
            var $product = $(event.target).parents(product);
            var $productCnt = $product.find(productCnt).val();

            if ($productCnt <= 0) {
                return false;
            }


            var $productPrice = $product.data('initial-price');
            var $productPriceField = $product.find(productPrice);
            var $productOptionsList = $product.find(productOptionsList);
            var total = 0;


            total = $productPrice * $productCnt;

            $productOptionsList.each(function (idx, data) {
                var initialCnt = $(data).data('initial-cnt');
                var initialPrice = $(data).data('initial-price');
                var cnt = $(data).val();

                if (cnt > initialCnt) {
                    total += (cnt - initialCnt) * initialPrice;
                }
            });

            $productPriceField.text(total);
        }

        this.OptionGenerate = function (event) {
            _recalcProductPrice(event);

            var $product = $(event.target).parents(product);
            var _productOptionList = $product.find(productOptionsList);
            var _productOptionListJson = {};

            _productOptionList.each(function(index, element) {
                _productOptionListJson[$(element).attr('name')] = $(element).val();
            });

            $product.find(productOptionsHideField).val(JSON.stringify(_productOptionListJson));
        };

        _initEvents();
    };
})();