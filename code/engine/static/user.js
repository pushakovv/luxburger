(function () {
    window.User = new function () {
        var jsLoginForm = '.js-login-form';
        var jsAuthForm = '.js-auth-form';
        var jsConfirmForm = '.js-confirm-form';

        var authUrl = '/auth/';
        var loginUrl = '/auth/confirm/';

        function _initEvents() {
            $(jsLoginForm);
        }

        this.Login = function(event) {
            event.preventDefault();
            var formData = $(jsAuthForm).serialize();
            $.post(authUrl, formData, function (res) {
                $(jsAuthForm).parents('main').append(res);
                $(jsAuthForm).remove();
            });
        };

        this.Auth = function(event) {
            event.preventDefault();
            var formData = $(jsConfirmForm).serialize();
            $.post(loginUrl, formData, function (res) {
                var massege = '<p>' + res.user_name + ' <a href="/auth/logout/">Logout</a></p>';
                $(jsConfirmForm).parents('main').append(massege);
                $(jsConfirmForm).remove();
            });
        };

        _initEvents();
    };
})();