from django.contrib.auth.models import User
from django.db import models
from django.utils.timezone import now


class Ingredient(models.Model):
    name = models.CharField(max_length=50)
    nutritional_value = models.CharField(max_length=30)
    weight = models.FloatField()
    cnt = models.PositiveSmallIntegerField(default=1)
    price = models.FloatField(null=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=30)
    price = models.FloatField()
    image = models.ImageField(null=True)
    description = models.CharField(max_length=75)
    long_description = models.CharField(max_length=250, null=True)
    status = models.BooleanField(default=True)
    composition = models.ManyToManyField(Ingredient)

    def __str__(self):
        return self.name


class Cart(models.Model):
    user_id = models.CharField(max_length=50)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    product_options_list = models.CharField(max_length=250)
    cnt = models.PositiveSmallIntegerField(default=1)
    total_price = models.FloatField(default=0)

    def __str__(self):
        return self.product_id.name


class Profile(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=30, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=11, null=True, blank=True)
    born = models.DateField(null=True, blank=True)
    points = models.PositiveSmallIntegerField(default=0, blank=True)
    refferal_link = models.CharField(max_length=30, null=True, blank=True)


class Address(models.Model):
    user_id = models.CharField(max_length=250, null=True)
    address = models.CharField(max_length=250, null=True)
    apartments = models.PositiveSmallIntegerField(null=True, blank=True)
    entrance = models.PositiveSmallIntegerField(null=True, blank=True)
    floor = models.PositiveSmallIntegerField(null=True, blank=True)


class Coupon(models.Model):
    coupon_name = models.CharField(max_length=15)
    percentage = models.CharField(max_length=15, null=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.coupon_name


class Order(models.Model):
    discount_id = models.ForeignKey(Coupon, on_delete=models.CASCADE, null=True, blank=True)
    address_id = models.ForeignKey(Address, on_delete=models.CASCADE)
    user_id = models.CharField(max_length=250)
    cart = models.TextField()
    payment_type = models.SmallIntegerField()
    cash_back = models.SmallIntegerField(null=True, blank=True)
    status = models.SmallIntegerField(default=0)
    date_order_created = models.DateTimeField(default=now)
    date_order_precessed = models.DateTimeField(null=True)
    date_order_prepared = models.DateTimeField(null=True)
    date_order_delivered = models.DateTimeField(null=True)

    def __str__(self):
        return self.cart


class FeedBack(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    rating = models.SmallIntegerField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.comment


class Transaction(models.Model):
    APPROVAL_CHOICES = (
        (u'1', u'Increase'),
        (u'2', u'Decrease'),
    )
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    points = models.SmallIntegerField(default=0)
    operation_type = models.CharField(max_length=1, choices=APPROVAL_CHOICES)
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)


class News(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    image = models.ImageField(null=True)

    def __str__(self):
        return self.title


class DiscountInfo(models.Model):
    text = models.TextField()
    image = models.ImageField(null=True)

    def __str__(self):
        return self.title


class Subscribe(models.Model):
    APPROVAL_CHOICES = (
        (u'1', u'News'),
        (u'2', u'Discount'),
        (u'3', u'Action'),
    )
    email = models.EmailField()
    type = models.CharField(max_length=1, choices=APPROVAL_CHOICES)
    created = models.DateTimeField(default=now)

    def __str__(self):
        return self.email


class Contact(models.Model):
    phone = models.CharField(max_length=16)
    email = models.EmailField()
    address = models.CharField(max_length=120)
    operating_time = models.CharField(max_length=20)
    text = models.TextField()

    def __str__(self):
        return self.address
