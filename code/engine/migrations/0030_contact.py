# Generated by Django 2.2 on 2019-06-04 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0029_subscribe'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(max_length=16)),
                ('email', models.EmailField(max_length=254)),
                ('address', models.CharField(max_length=120)),
                ('operating_time', models.CharField(max_length=20)),
                ('text', models.TextField()),
            ],
        ),
    ]
