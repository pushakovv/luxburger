# Generated by Django 2.2 on 2019-05-29 12:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0023_auto_20190528_1548'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='comment',
        ),
        migrations.RemoveField(
            model_name='order',
            name='rating',
        ),
        migrations.AlterField(
            model_name='order',
            name='cash_back',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='discount_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='engine.Coupon'),
        ),
        migrations.CreateModel(
            name='FeedBack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rating', models.SmallIntegerField(blank=True, null=True)),
                ('comment', models.TextField(blank=True, null=True)),
                ('order_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='engine.Order')),
            ],
        ),
    ]
