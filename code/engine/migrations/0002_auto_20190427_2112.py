# Generated by Django 2.2 on 2019-04-27 21:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='burger',
            name='description',
            field=models.CharField(max_length=255),
        ),
    ]
