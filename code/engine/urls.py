from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    path('auth/', views.auth, name='auth'),
    path('auth/logout/', views.account_logout, name='account_logout'),
    path('auth/confirm/', views.confirm, name='confirm'),

    path('product/<int:product_id>/', views.detail, name='detail'),

    path('menu/', views.menu, name='menu'),

    path('cart/', views.cart, name='cart'),
    path('cart/add/', views.cart_add, name='cart_add'),
    path('cart/update/', views.cart_update, name='cart_update'),
    path('cart/remove/', views.cart_remove, name='cart_remove'),

    path('profile/', views.profile, name='profile'),
    path('profile/save/', views.profile_save, name='profile_save'),

    path('address/remove/', views.address_remove, name='address_remove'),

    path('coupon/set/', views.coupon_set, name='coupon_set'),

    path('order/', views.order, name='order'),
    path('order/repeat/', views.order_repeat, name='order_repeat'),

    path('orders/', views.orders_control, name='orders_control'),

    path('news/<int:news_id>/', views.news_detail, name='news_detail'),
    path('news/', views.news, name='news'),

    path('contacts/', views.contacts, name='contacts'),

    path('subscribe/', views.subscribe, name='subscribe'),

    path('discounts/', views.discount, name='discount'),

    path('feedback/', views.feedback, name='feedback'),
]
