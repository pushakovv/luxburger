FROM python:3
RUN mkdir /luxburger
WORKDIR /luxburger
COPY code /luxburger/
RUN pip install -r requirements.txt
CMD ["django-admin", "startproject", "luxburger"]