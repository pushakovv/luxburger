## LuxBurger

### System requirements:
* Python >= 3.7.0
* Mysql >= 5.7.0
* Docker >= 18.09.0
* Docker Compose == 1.23.2

### Start project:
* For start project use ```docker-compose up -d```
* Nex step import DB data from ```dump``` directory
* Ensure what you are use correct creds in ```.env``` file

### Notes:
* First i was can`t connect to DB through Sequel Pro, after that i started use Grid DB from PyCharm
* For debug scripts through docker container read this [DOC](https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html)
