# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.10.10 (MySQL 5.7.14-log)
# Database: luxburger
# Generation Time: 2019-05-07 16:59:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table auth_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_group`;

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_group_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_group_permissions`;

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_permission`;

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`)
VALUES
	(1,'Can add cart',1,'add_cart'),
	(2,'Can change cart',1,'change_cart'),
	(3,'Can delete cart',1,'delete_cart'),
	(4,'Can view cart',1,'view_cart'),
	(5,'Can add ingredient',2,'add_ingredient'),
	(6,'Can change ingredient',2,'change_ingredient'),
	(7,'Can delete ingredient',2,'delete_ingredient'),
	(8,'Can view ingredient',2,'view_ingredient'),
	(9,'Can add burger',3,'add_burger'),
	(10,'Can change burger',3,'change_burger'),
	(11,'Can delete burger',3,'delete_burger'),
	(12,'Can view burger',3,'view_burger'),
	(13,'Can add log entry',4,'add_logentry'),
	(14,'Can change log entry',4,'change_logentry'),
	(15,'Can delete log entry',4,'delete_logentry'),
	(16,'Can view log entry',4,'view_logentry'),
	(17,'Can add permission',5,'add_permission'),
	(18,'Can change permission',5,'change_permission'),
	(19,'Can delete permission',5,'delete_permission'),
	(20,'Can view permission',5,'view_permission'),
	(21,'Can add group',6,'add_group'),
	(22,'Can change group',6,'change_group'),
	(23,'Can delete group',6,'delete_group'),
	(24,'Can view group',6,'view_group'),
	(25,'Can add user',7,'add_user'),
	(26,'Can change user',7,'change_user'),
	(27,'Can delete user',7,'delete_user'),
	(28,'Can view user',7,'view_user'),
	(29,'Can add content type',8,'add_contenttype'),
	(30,'Can change content type',8,'change_contenttype'),
	(31,'Can delete content type',8,'delete_contenttype'),
	(32,'Can view content type',8,'view_contenttype'),
	(33,'Can add session',9,'add_session'),
	(34,'Can change session',9,'change_session'),
	(35,'Can delete session',9,'delete_session'),
	(36,'Can view session',9,'view_session'),
	(37,'Can add products',10,'add_products'),
	(38,'Can change products',10,'change_products'),
	(39,'Can delete products',10,'delete_products'),
	(40,'Can view products',10,'view_products'),
	(41,'Can add product',10,'add_product'),
	(42,'Can change product',10,'change_product'),
	(43,'Can delete product',10,'delete_product'),
	(44,'Can view product',10,'view_product'),
	(45,'Can add profile',11,'add_profile'),
	(46,'Can change profile',11,'change_profile'),
	(47,'Can delete profile',11,'delete_profile'),
	(48,'Can view profile',11,'view_profile'),
	(49,'Can add address',12,'add_address'),
	(50,'Can change address',12,'change_address'),
	(51,'Can delete address',12,'delete_address'),
	(52,'Can view address',12,'view_address');

/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user`;

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`)
VALUES
	(1,'pbkdf2_sha256$150000$1tTUXYlPizc1$68ziOmvll2LQrvQfPtvKDCTopJ1qqFpkkU/k6vtpblo=','2019-05-07 14:09:25.553610',1,'pushakov','','','pushakov@yandex.ru',1,1,'2019-05-02 13:07:20.371615'),
	(2,'392284',NULL,0,'89001231212','','','',0,0,'2019-05-02 13:08:14.315248'),
	(3,'pbkdf2_sha256$150000$6aZ3ZsH5Eki9$M8F7wzRYJPjHKyhNO7qYDY7Uj0nu548vQmXeqHn0uNk=',NULL,0,'89001234567','','','',0,1,'2019-05-02 14:25:31.891774'),
	(4,'pbkdf2_sha256$150000$VvvUgDaqiqmp$1OZqT2fkNKsR4bgft6g8ZvI//ONzUd2NwiH1OGrasmg=',NULL,0,'89001112233','','','',0,1,'2019-05-02 14:28:04.838484'),
	(5,'pbkdf2_sha256$150000$XsqwA2tfBMTJ$wW0AbtMANH2weoIqA74PKZTBIZKePuKcZxXjVq2zmKk=','2019-05-04 09:54:32.890466',0,'89004445588','','','',0,1,'2019-05-02 14:34:01.330154'),
	(6,'pbkdf2_sha256$150000$C53r3KkzAMPq$vmOXvZhauHf9meoUNYlbbrCj93VQ3V00YcFdTOvdT/8=','2019-05-04 20:02:03.469860',0,'89006999682','','','',0,1,'2019-05-04 20:01:45.475155');

/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user_groups`;

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_user_user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_user_user_permissions`;

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table django_admin_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_admin_log`;

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`)
VALUES
	(1,'2019-05-02 13:08:00.671212','1','Сыр \"Чеддер\"',1,'[{\"added\": {}}]',2,1),
	(2,'2019-05-02 13:08:02.489082','1','ТВОЙбургер',1,'[{\"added\": {}}]',3,1),
	(3,'2019-05-03 20:10:10.631946','2','Красная икра',1,'[{\"added\": {}}]',2,1),
	(4,'2019-05-03 20:10:12.643124','1','Новый русский',1,'[{\"added\": {}}]',10,1),
	(5,'2019-05-07 14:17:44.984756','3','Сыр \"Чеддер\"',1,'[{\"added\": {}}]',2,1),
	(6,'2019-05-07 14:18:09.311234','4','Котлета из \"Мраморной Говядины\"',1,'[{\"added\": {}}]',2,1),
	(7,'2019-05-07 14:18:39.078237','5','Соус \"Blue chase\"',1,'[{\"added\": {}}]',2,1),
	(8,'2019-05-07 14:19:03.595792','1','Новый русский',2,'[{\"changed\": {\"fields\": [\"long_description\", \"composition\"]}}]',10,1);

/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_content_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_content_type`;

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;

INSERT INTO `django_content_type` (`id`, `app_label`, `model`)
VALUES
	(4,'admin','logentry'),
	(6,'auth','group'),
	(5,'auth','permission'),
	(7,'auth','user'),
	(8,'contenttypes','contenttype'),
	(12,'engine','address'),
	(3,'engine','burger'),
	(1,'engine','cart'),
	(2,'engine','ingredient'),
	(10,'engine','product'),
	(11,'engine','profile'),
	(9,'sessions','session');

/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_migrations`;

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`)
VALUES
	(1,'contenttypes','0001_initial','2019-05-02 09:39:34.709590'),
	(2,'auth','0001_initial','2019-05-02 09:39:34.950656'),
	(3,'admin','0001_initial','2019-05-02 09:39:35.290008'),
	(4,'admin','0002_logentry_remove_auto_add','2019-05-02 09:39:35.374459'),
	(5,'admin','0003_logentry_add_action_flag_choices','2019-05-02 09:39:35.396822'),
	(6,'contenttypes','0002_remove_content_type_name','2019-05-02 09:39:35.481157'),
	(7,'auth','0002_alter_permission_name_max_length','2019-05-02 09:39:35.516229'),
	(8,'auth','0003_alter_user_email_max_length','2019-05-02 09:39:35.559761'),
	(9,'auth','0004_alter_user_username_opts','2019-05-02 09:39:35.571969'),
	(10,'auth','0005_alter_user_last_login_null','2019-05-02 09:39:35.613198'),
	(11,'auth','0006_require_contenttypes_0002','2019-05-02 09:39:35.615583'),
	(12,'auth','0007_alter_validators_add_error_messages','2019-05-02 09:39:35.626892'),
	(13,'auth','0008_alter_user_username_max_length','2019-05-02 09:39:35.665468'),
	(14,'auth','0009_alter_user_last_name_max_length','2019-05-02 09:39:35.704302'),
	(15,'auth','0010_alter_group_name_max_length','2019-05-02 09:39:35.776376'),
	(16,'auth','0011_update_proxy_permissions','2019-05-02 09:39:35.785821'),
	(17,'engine','0001_initial','2019-05-02 09:39:35.945702'),
	(18,'engine','0002_auto_20190427_2112','2019-05-02 09:39:36.050858'),
	(19,'sessions','0001_initial','2019-05-02 09:39:36.130412'),
	(20,'engine','0003_auto_20190503_1831','2019-05-03 18:31:32.473862'),
	(21,'engine','0004_auto_20190503_1845','2019-05-03 18:45:13.098143'),
	(22,'engine','0005_auto_20190503_2001','2019-05-03 20:03:05.934100'),
	(23,'engine','0006_remove_cart_product_id','2019-05-03 20:43:38.927505'),
	(24,'engine','0007_remove_burger_composition','2019-05-03 20:45:09.700443'),
	(25,'engine','0008_delete_burger','2019-05-03 20:45:44.083895'),
	(26,'engine','0009_auto_20190503_2046','2019-05-03 20:46:31.540163'),
	(27,'engine','0010_cart_product_id','2019-05-03 20:53:22.662012'),
	(28,'engine','0011_auto_20190504_1923','2019-05-04 19:23:36.194272'),
	(29,'engine','0012_auto_20190505_1125','2019-05-05 11:26:34.560596'),
	(30,'engine','0013_auto_20190505_1133','2019-05-05 11:33:27.573174'),
	(31,'engine','0014_cart_cnt','2019-05-05 12:04:26.990490'),
	(32,'engine','0015_auto_20190507_1413','2019-05-07 14:13:59.806757');

/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table django_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `django_session`;

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`)
VALUES
	('26pf599wdw7yyf9fmtqtge4nenw1ksii','N2Q1MjcwMzk5ZGY2MWQyMDEwYjAxZTVkZTYxNWFjOTU4YzBhMDZlMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzE2ZTIxMDU0MDE5NDU1ODZhNjU5YzVkNDlmY2YyOTk3NGQ2ZDdkIn0=','2019-05-16 13:07:32.236800'),
	('c3fh96fow7pxu584wmzi3013zqpry9qh','YjA2MzQ3MjQ0YzU5ZWM3ZTcxNTM2YjMwMzcxODgwM2QwYTgxMzI4ZDp7fQ==','2019-05-16 15:20:08.260770'),
	('dgjwvovuk8v7mzb46vy1j1hykrf5ouef','YjA2MzQ3MjQ0YzU5ZWM3ZTcxNTM2YjMwMzcxODgwM2QwYTgxMzI4ZDp7fQ==','2019-05-16 15:14:20.462335'),
	('gq2wwa8lqud57jlig51vpm9s83luair5','YjA2MzQ3MjQ0YzU5ZWM3ZTcxNTM2YjMwMzcxODgwM2QwYTgxMzI4ZDp7fQ==','2019-05-16 15:12:50.226468'),
	('h5m4472bgu9v4j8q8dkvhkvz6s4bgd3j','YjA2MzQ3MjQ0YzU5ZWM3ZTcxNTM2YjMwMzcxODgwM2QwYTgxMzI4ZDp7fQ==','2019-05-16 15:13:37.268307'),
	('qz4u86nbee60ah6b8sp11uzto5zqgdg7','N2Q1MjcwMzk5ZGY2MWQyMDEwYjAxZTVkZTYxNWFjOTU4YzBhMDZlMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzE2ZTIxMDU0MDE5NDU1ODZhNjU5YzVkNDlmY2YyOTk3NGQ2ZDdkIn0=','2019-05-21 14:09:25.555900');

/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table engine_address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_address`;

CREATE TABLE `engine_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(250) DEFAULT NULL,
  `apartments` smallint(5) unsigned DEFAULT NULL,
  `entrance` smallint(5) unsigned DEFAULT NULL,
  `floor` smallint(5) unsigned DEFAULT NULL,
  `user_id_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `engine_address_user_id_id_d5dcc033_fk_auth_user_id` (`user_id_id`),
  CONSTRAINT `engine_address_user_id_id_d5dcc033_fk_auth_user_id` FOREIGN KEY (`user_id_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table engine_cart
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_cart`;

CREATE TABLE `engine_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `product_options_list` varchar(250) NOT NULL,
  `product_id_id` int(11) DEFAULT NULL,
  `cnt` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `engine_cart_product_id_id_bd390147_fk_engine_product_id` (`product_id_id`),
  CONSTRAINT `engine_cart_product_id_id_bd390147_fk_engine_product_id` FOREIGN KEY (`product_id_id`) REFERENCES `engine_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `engine_cart` WRITE;
/*!40000 ALTER TABLE `engine_cart` DISABLE KEYS */;

INSERT INTO `engine_cart` (`id`, `user_id`, `product_options_list`, `product_id_id`, `cnt`)
VALUES
	(10,'b747fe3e-df6c-46fd-b995-0809ad7005b4','',1,1),
	(11,'b747fe3e-df6c-46fd-b995-0809ad7005b4','',1,1),
	(13,'5','',1,1),
	(14,'5','',1,1),
	(16,'6','',1,2),
	(17,'6','',1,3);

/*!40000 ALTER TABLE `engine_cart` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table engine_ingredient
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_ingredient`;

CREATE TABLE `engine_ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `nutritional_value` varchar(30) NOT NULL,
  `weight` double NOT NULL,
  `cnt` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `engine_ingredient` WRITE;
/*!40000 ALTER TABLE `engine_ingredient` DISABLE KEYS */;

INSERT INTO `engine_ingredient` (`id`, `name`, `nutritional_value`, `weight`, `cnt`)
VALUES
	(2,'Красная икра','30',34,1),
	(3,'Сыр \"Чеддер\"','80',10,2),
	(4,'Котлета из \"Мраморной Говядины\"','120',40,1),
	(5,'Соус \"Blue chase\"','80',30,1);

/*!40000 ALTER TABLE `engine_ingredient` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table engine_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_product`;

CREATE TABLE `engine_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(75) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `long_description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `engine_product` WRITE;
/*!40000 ALTER TABLE `engine_product` DISABLE KEYS */;

INSERT INTO `engine_product` (`id`, `name`, `price`, `description`, `status`, `long_description`)
VALUES
	(1,'Новый русский',450,'Самый дорогой бургер',1,'Детальное описание товара');

/*!40000 ALTER TABLE `engine_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table engine_product_composition
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_product_composition`;

CREATE TABLE `engine_product_composition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `engine_products_composit_products_id_ingredient_i_6ce406ae_uniq` (`product_id`,`ingredient_id`),
  KEY `engine_products_comp_ingredient_id_90b586a9_fk_engine_in` (`ingredient_id`),
  CONSTRAINT `engine_product_compo_product_id_4cd51298_fk_engine_pr` FOREIGN KEY (`product_id`) REFERENCES `engine_product` (`id`),
  CONSTRAINT `engine_products_comp_ingredient_id_90b586a9_fk_engine_in` FOREIGN KEY (`ingredient_id`) REFERENCES `engine_ingredient` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `engine_product_composition` WRITE;
/*!40000 ALTER TABLE `engine_product_composition` DISABLE KEYS */;

INSERT INTO `engine_product_composition` (`id`, `product_id`, `ingredient_id`)
VALUES
	(1,1,2),
	(2,1,3),
	(3,1,4),
	(4,1,5);

/*!40000 ALTER TABLE `engine_product_composition` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table engine_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_profile`;

CREATE TABLE `engine_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `born` date DEFAULT NULL,
  `user_id_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `engine_profile_user_id_id_99261d0f_fk_auth_user_id` (`user_id_id`),
  CONSTRAINT `engine_profile_user_id_id_99261d0f_fk_auth_user_id` FOREIGN KEY (`user_id_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `engine_profile` WRITE;
/*!40000 ALTER TABLE `engine_profile` DISABLE KEYS */;

INSERT INTO `engine_profile` (`id`, `name`, `email`, `phone`, `born`, `user_id_id`)
VALUES
	(1,NULL,NULL,NULL,NULL,6);

/*!40000 ALTER TABLE `engine_profile` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
